# Twitter Account Creator with Python Scraper

This scripts will help you to create the new twitter account more easily.
It is implemented with two services [CapSolver](https://www.capsolver.com) and [KOPECHKA Store](https://kopeechka.store).

## How to setting up the scraper

`config/config.json`

- threads:  Number of accounts you want to create
- capsolver_key:  [CapSolver](https://www.capsolver.com)
- yescaptcha_key:  [CapSolver](https://www.capsolver.com) (Optional)
- kopeechka_key:  [KOPECHKA Store](https://kopeechka.store)
- account_password:  The password of the new twitter account

## How to install and run

```bash
pip install -r requirements.txt
```

```bash
python main.py
```

## How to use the saved twitter accounts

You can see the saved twitter accounts from `output/accounts.txt`