class Endpoint:
    BASE_LOGIN_FLOW = 'https://api.twitter.com/1.1/onboarding/task.json'
    GUEST_TOKEN_FLOW = 'https://api.twitter.com/1.1/guest/activate.json'
    VERIFICATION_FLOW = 'https://api.twitter.com/1.1/onboarding/begin_verification.json'
    BASE_TOKEN_FLOW = 'https://api-31-0-0.twitter.com/1.1/account/settings.json'